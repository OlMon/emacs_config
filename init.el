;; init.el --- Emacs configuration 


(add-hook 'emacs-startup-hook 'my/set-gc-threshold)
(defun my/set-gc-threshold ()
  "Reset `gc-cons-threshold' to its default value."
  (setq gc-cons-threshold (* 8 1024 1024)
        gc-cons-percentage 0.1))

;; Bug with auctex
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Load my literal config, tangle if needed
(let ((conf-el (concat user-emacs-directory "conf.el"))
      (conf-org (concat user-emacs-directory "conf.org")))
  (if (and (file-exists-p conf-el)
           (file-newer-than-file-p conf-el conf-org))
      (load-file conf-el)
    (let ((org-babel-default-header-args '((:noweb   . "yes"))))
      (require 'ob-tangle)
      (org-babel-tangle-file conf-org)
      (load-file conf-el))))

(defun ol/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                    (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'ol/display-startup-time)
