#+TITLE: Needed Symlinks
#+DESCRIPTION: Description of all needed Symlinks

* dict
Symlink to my personal dict dir
* elfeed
Symlink to my personal dict dir
* knowdb
Symlink to personal knowledge database
* mu4e
Directory with all mu4e stuff
** mailiases
Symlink to mailalias file
** pw
Symlink to folder with mail passwords
** signatures
Symlink to directory with signatures
* org
Directory with all org symlinks
** org-agenda
Symlink to my org agenda directory
** org-mobile
Symlink to my org mobile dorectory
** org-captures
Directory with all symlinks to files used in captures
*** bookmarks.org
*** journal.org
*** phd.org
*** quicknotes.org
*** todo.org
*** work.org
* persondb
Directory with all org documents about persons
