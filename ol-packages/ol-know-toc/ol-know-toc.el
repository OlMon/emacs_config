;;; ol-know-toc.el --- Table of content for custom knowdb  -*- lexical-binding: t; -*-

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marco Pawłowski
;; Version: 0.5
;; Package-Requires: ((org "9.6"))
;; Keywords: convinient

;;; Commentary:

;; Package provides simple structure to create tables of content for custom know db

;;; Code:

(require 'org)
(require 'org-element)
(require 'org-compat)

(defvar ol-know-toc-knowdb-dir "" "Define root dir of the knowledge base.")

(defun ol-know-toc-get-keyword-key-value (kwd)
  "Get keyword KWD from list."
  (let ((data (cadr kwd)))
    (list (plist-get data :key)
          (plist-get data :value))))

(defun ol-know-toc-org-current-buffer-get-prop (prop)
  "Collect property PROP from current org buffer."
  (nth 1
       (assoc prop
              (org-element-map
                  (org-element-parse-buffer 'greater-element)
                  '(keyword)
                #'ol-know-toc-get-keyword-key-value))))

(defun ol-know-toc-org-file-get-prop (file prop)
  "Get all properties PROP from FILE."
  (with-temp-buffer
    (insert-file-contents file)
    (ol-know-toc-org-current-buffer-get-prop prop)))


(defun ol-know-toc-dir-walk (root)
  "Create a tree from all directories and org files, under ROOT."
  (let ((files '()) ;empty list to store results
        (current-list (directory-files root t)))
    ;;process current-list
    (while current-list
      (let ((fn (car current-list))) ; get next entry
        (cond
         ;; regular files
         ((and (file-regular-p fn)
               (not (string= "." (substring (file-name-base fn) 0 1))) ;; don't include hidden files
               (not (string= (buffer-name (current-buffer))
                             (file-relative-name fn "."))))
          (push (file-relative-name fn ".") files))
         ;; directories
         ((and
           (file-directory-p fn)
           ;; ignore . and ..
           (not (string-equal ".." (substring fn -2)))
           (not (string-equal "." (substring fn -1))))
          ;; we have to recurse into this directory
          (setq files (append files (list (ol-know-toc-dir-walk (concat fn "/")))))))
        ;; cut list down by an element
        (setq current-list (cdr current-list))))
    (list :dir (file-relative-name root) :files files)))


(defun ol-know-toc-get-headings (file)
  "Get all org-headings from FILE."
  (mapconcat 'identity
             (org-map-entries
              (lambda () (let ((heading (substring-no-properties (org-get-heading t t nil t)))
                               (id (org-id-get-create)))
                           (concat (format "%s- " (make-string (* (1- (org-current-level)) 2) ? ))
                                   (format "[[id:%s][%s%s]]"
                                           id
                                           heading
                                           ;; get the count for sorting trees
                                           (let ((scount (1- (length (org-map-entries 'org-get-heading "+sorting" 'tree)))))
                                                  (if (> scount 0)
                                                      (concat " ~" (int-to-string scount) "~")
                                                    ""))))))
              t
              (list file)) "\n"))



(defun ol-know-toc-print-toc (files indent)
  "Print table of content from FILES, with a given indention INDENT."
  (let ((toc ""))
      (let ((ele files))
        (setq toc (concat toc (format "%s %s\n%s" indent
                                      (let ((dir (plist-get ele :dir)))
                                        (if (string= dir ".")
                                            "Table of Content"
                                          dir))
                                      (ol-know-toc-print-sub-toc (plist-get ele :files) (concat indent "*")))))
        )
    toc))

(defun ol-know-toc-print-sub-toc (files indent)
  "Print the sub elements of a table of contents, from FILES with indentation INDENT."
  (let ((toc ""))
  (while files
    (let ((ele (car files)))
      (cond
       ((listp ele)
        (setq toc (concat toc (ol-know-toc-print-toc ele indent))))
       ((and (stringp ele) (string= "org" (file-name-extension ele)))
        (setq toc (concat toc indent " " (or (ol-know-toc-org-file-get-prop ele "TITLE") (file-name-base ele)) "\n"))
        (setq toc (concat toc (ol-know-toc-get-headings ele) "\n")))
       ((stringp ele)
        (setq toc (concat toc (format "%s: [[file:%s][%s]]" (capitalize (file-name-extension ele)) (file-name-directory ele) (file-name-base ele)) "\n")))
       )
      (setq files (cdr files))))
  toc))



(defun ol-know-toc-print-knowndb-toc (toc)
  "Prints the table of content into the the buffer after erasing the current table of content."
    (erase-buffer)
    (insert (format "[[elisp:(progn (grep-compute-defaults) (rgrep (completing-read \"Search for regex: \" nil) \"*.org\" \"%s\"))][Search]]\n" default-directory))
    (insert "[[elisp:(progn (occur \"To Sort ~\" nil `((,(org-first-headline-recenter) . ,(point-max)))))][Find To Sorts]]\n")
    (insert toc))

;;;###autoload
(defun ol-know-toc-knowndb-toc ()
  "Function to create a table of content, of my personal knowledge databse."
  (interactive)
  (let ((default-directory ol-know-toc-knowdb-dir))
    (switch-to-buffer (get-buffer-create "*toc*"))
    (ol-know-toc-print-knowndb-toc (ol-know-toc-print-toc (ol-know-toc-dir-walk ".") "*"))
    (org-id-update-id-locations (directory-files-recursively ol-know-toc-knowdb-dir "org$"))
    (org-mode)
    (setq-local org-confirm-elisp-confirm-function nil)
    (org-cycle '(64))
    (read-only-mode)
    (use-local-map (copy-keymap org-mode-map))
    (local-set-key "q" 'kill-buffer)
    (local-set-key "g" 'ol-know-toc-refresh-toc-buffer)))

(defun ol-know-toc-refresh-toc-buffer ()
  "function to refresh the toc buffer"
  (interactive)
  (let ((default-directory ol-know-toc-knowdb-dir))
    (read-only-mode -1)
    (save-excursion
      (ol-know-toc-print-knowndb-toc (ol-know-toc-print-toc (ol-know-toc-dir-walk ".") "*"))
      (org-id-update-id-locations (directory-files-recursively ol-know-toc-knowdb-dir "org$"))
      (read-only-mode 1))))

(provide 'ol-know-toc)
;;; ol-know-toc.el ends here
