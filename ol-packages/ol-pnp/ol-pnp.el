;;; ol-pnp.el --- Pan and paper battles  -*- lexical-binding: t; -*-

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marco Pawłowski
;; Version: 0.5
;; Package-Requires: ((org "9.6"))
;; Keywords: convinient, fun

;;; Commentary:

;; Package provides simple structure to create tables for a pan and paper battle.
;;
;; Simple link strucutre to use inside an org buffer.
;; [[elisp:(ol-pnp-start-battle-from-link '("martin" "emi")'("goblinA" "goblinB"))][Battle]]

;;; Code:

(require 'org)
(require 'org-table)

(defvar ol-pnp-round-counter 0
  "The number of the current round.")

(defvar ol-pnp-round-position 1
  "The position in the current round.")

(defvar ol-pnp-round-indicator ">>>"
  "The indicator for the current round position.")

(defvar ol-pnp-columns '("Round 0" "Name" "Init" "HP" "max. HP" "Status" "Notes")
  "The names of the columns.")

(defvar ol-pnp-round-column 1
  "Index of the round column.")

(defvar ol-pnp-name-column 2
  "Index of the name column.")

(defvar ol-pnp-initiative-column 3
  "Index of the rolled initiative column.")

(defvar ol-pnp-hp-column 4
  "Index of the HP column.")

(defvar ol-pnp-max-hp-column 5
  "Index of the max HP column.")

(defvar ol-pnp-status-column 6
  "Index of the status column.")

(defvar ol-pnp-notes-column 7
  "Index of the notes column.")


(defun ol-pnp-start-battle-from-link (players enemies)
  "Create a table with PLAYERS and ENEMIES and start the transient."
  (interactive)
  (move-end-of-line 1)
  (newline 2)
  (save-excursion (insert (ol-pnp-create-table players enemies)))
  (ol-pnp-init-round-counter)
  (ol-pnp-transient))

(defun ol-pnp-create-link (players enemies &optional desc)
  "Create a link which acitivates a fight.
It includes PLAYERS ENEMIES with optional description DESC."
  (interactive)
  (org-link-make-string (format "elisp:(ol-pnp-start-battle-from-link '%S '%S)" players enemies) (format "%s" (or desc "Battle"))))

(defun ol-pnp-create-table (players enemies)
  "Create the table with PLAYERS and ENEMIES."
  (interactive)
  (with-temp-buffer
    (let ((players (append players enemies)))
      (org-table-create (format "%sx%s" (length ol-pnp-columns) (+ 1 (length players))))
      (ol-pnp-fill-header-row)
      (ol-pnp-fill-name-column players)
      (buffer-string))))

(defun ol-pnp-fill-name-column (participants)
  "Fill column with the names of the PARTICIPANTS."
  (let ((p participants)
        (row 2)) ;; Start from first data-line
    (while p
      (org-table-put row ol-pnp-name-column (string-replace "] [" "][" (format "%S" (car p))))
      (setq row (+ 1 row))
      (setq p (cdr p)))
    (org-table-align)))

(defun ol-pnp-round-position ()
  "Return the round position."
  (+ 1 ol-pnp-round-position))

(defun ol-pnp-fill-header-row ()
  "Fill the table with the header information."
  (let ((colnames ol-pnp-columns)
        (col 1))
    (while colnames
      (org-table-put 1 col (car colnames))
      (setq col (+ 1 col))
      (setq colnames (cdr colnames)))
    (org-table-align)))

(defun ol-pnp-init-round-counter ()
  "Initialize the round counter."
  (setq ol-pnp-round-counter 0)
  (ol-pnp-inc-round-counter))

(defun ol-pnp-inc-round-counter ()
  "Increment the round counter."
  (setq ol-pnp-round-counter (+ 1 ol-pnp-round-counter))
  (org-table-put 1 ol-pnp-round-column (format "Round %s" ol-pnp-round-counter) t))

(defun ol-pnp-clear-round-indicator ()
  "Clears round indicator,"
  (org-table-put (ol-pnp-round-position) 1 "" t))

(defun ol-pnp-init-round-indicator ()
  "Initialize the round indicator."
  (interactive)
  (when (org-at-table-p)
    (ol-pnp-clear-round-indicator)
    (org-table-put 2 1 ol-pnp-round-indicator t)
    (setq ol-pnp-round-position 1)))

(defun ol-pnp-indicator-not-last-line-p ()
  "Check if indicator is not in the last line."
  (save-excursion
    (org-table-goto-line (+ 1 (ol-pnp-round-position)))))

(defun ol-pnp-increment-round-indicator ()
  "Incre,ent the round indicator."
  (interactive)
  (if (ol-pnp-indicator-not-last-line-p)
      (progn (org-table--swap-cells (ol-pnp-round-position) 1 (+ 1 (ol-pnp-round-position)) 1)
             (setq ol-pnp-round-position (ol-pnp-round-position)))
    (org-table--swap-cells (ol-pnp-round-position) 1 2 1)
    (setq ol-pnp-round-position 1)
    (ol-pnp-inc-round-counter))
  (org-table-align))

(defun ol-pnp-pointer-not-last-line-p ()
  "Check if pointer is not in the last line."
  (save-excursion
    (forward-line)
    (org-at-table-p)))

(defun ol-pnp-goto-first-dline ()
  "Goto the first line after the headline."
  (org-table-goto-line 2))

(defun ol-pnp-move-down-dline ()
  "Move down a data line if there is one more."
  (interactive)
  (when (ol-pnp-pointer-not-last-line-p)
    (forward-line)))

(defun ol-pnp-move-up-dline ()
  "Move up a data line if there is one more."
  (interactive)
  (when (> (org-table-current-dline) 2)
    (forward-line -1)))


(defun ol-pnp-apply-damage ()
  "Ask for damage and apply to the current line."
  (interactive)
  (save-excursion
    (let ((dmg (string-to-number (completing-read "How much dmg to apply: " nil)))
          (hp (string-to-number (org-table-get-field ol-pnp-hp-column))))
      (org-table-put (org-table-current-dline) ol-pnp-hp-column (format "%s" (- hp dmg)) t))))

(defun ol-pnp-sort-by-initative ()
  "Sort the table by the initiative."
  (interactive)
  (save-excursion
    (org-table-goto-column ol-pnp-initiative-column)
    (org-table-sort-lines nil ?n)))

(defun ol-pnp-set-hp ()
  "Set the HP for the current line."
  (interactive)
  (save-excursion
    (let ((hp (completing-read "HP: " nil)))
      (org-table-put (org-table-current-dline) ol-pnp-hp-column hp t))))

(defun ol-pnp-set-max-hp ()
  "Set the max HP for the current line."
  (interactive)
  (save-excursion
    (let ((mhp (completing-read "Max HP: " nil)))
      (org-table-put (org-table-current-dline) ol-pnp-max-hp-column mhp t))))

(defun ol-pnp-set-max-hp-and-hp ()
  "Set the max HP and HP for the current line."
  (interactive)
  (save-excursion
    (let ((mhp (completing-read "Max HP & HP: " nil)))
      (org-table-put (org-table-current-dline) ol-pnp-max-hp-column mhp t)
      (org-table-put (org-table-current-dline) ol-pnp-hp-column mhp t))))

(defun ol-pnp-set-initiative ()
  "Set the initiative for the current line."
  (interactive)
  (save-excursion
    (let ((init (completing-read "Initiative: " nil)))
      (org-table-put (org-table-current-dline) ol-pnp-initiative-column init t))))

(defun ol-pnp-add-status ()
  "Add status for the current line."
  (interactive)
  (save-excursion
    (let ((status (completing-read "Status: " nil nil nil (org-table-get-field ol-pnp-status-column))))
      (org-table-put (org-table-current-dline) ol-pnp-status-column status t))))

(defun ol-pnp-add-notes ()
  "Add notes for the current line."
  (interactive)
  (save-excursion
    (let ((note (completing-read "Note: " nil nil nil (org-table-get-field ol-pnp-notes-column))))
      (org-table-put (org-table-current-dline) ol-pnp-notes-column note t))))

(defun ol-pnp-add-monster ()
  "Add a monster to the table."
  (interactive)
  (org-table-insert-row)
  (org-table-put (org-table-current-dline) ol-pnp-name-column
                 (completing-read "Monster Name: " nil) t)
  (ol-pnp-set-initiative)
  (ol-pnp-sort-by-initative))

(defun ol-pnp-point-at-link-p ()
  "Check if point is on link."
  (org-element-lineage
   (org-element-context)
   '(link) t))

(defun ol-pnp-name-is-link-p ()
  "Check if name is a link."
  (save-excursion
    (org-table-goto-column ol-pnp-name-column)
    (ol-pnp-point-at-link-p)))

(defun ol-pnp-open-link ()
  "Open link in current org table dline."
  (interactive)
  (when (ol-pnp-name-is-link-p)
    (org-table-goto-column ol-pnp-name-column)
    (org-open-at-point t)))

(defun ol-pnp-fill-hp-from-link ()
  "Fill HP column for a linked property."
  (interactive)
  (when (ol-pnp-name-is-link-p)
    (let ((mhp (ol-pnp-get-property-of-link "HP")))
      (org-table-put (org-table-current-dline) ol-pnp-max-hp-column mhp t)
      (org-table-put (org-table-current-dline) ol-pnp-hp-column mhp t))))

(defun ol-pnp-get-property-of-link (prop)
  "Get a property PROP from a linked heading."
  (interactive)
  (when (ol-pnp-name-is-link-p)
    (ol-pnp-goto-linked-name-and-call (lambda () (org-entry-get (point) prop)))))

(defun ol-pnp-goto-linked-name-and-call (func)
  "Call a function FUNC witharguments ARGS after visiting a linked name in Table."
  (save-window-excursion
    (save-excursion
      (org-table-goto-column ol-pnp-name-column)
      (org-open-at-point)
      (funcall func))))

(defun ol-pnp-get-all-info-from-link ()
  "Get all information of the link in current org table dline."
  (interactive)
  (message "%s" (ol-pnp-goto-linked-name-and-call (lambda ()
                                                    (org-open-at-point t)
                                                    (let ((b-point (point)))
                                                      (search-forward ":END:")
                                                      (move-end-of-line 1)
                                                      (buffer-substring b-point (point)))))))

(defun ol-pnp-die (sides)
  "General function to roll a die with SIDES sides."
  (+ 1 (random sides)))

(defun ol-pnp-d4 ()
  "Roll a D4 die."
  (interactive)
  (message "Rolled: %s" (ol-pnp-die 4)))

(defun ol-pnp-d6 ()
  "Roll a D6 die."
  (interactive)
  (message "Rolled: %s" (ol-pnp-die 6)))

(defun ol-pnp-d10 ()
  "Roll a D10 die."
  (interactive)
  (message "Rolled: %s" (ol-pnp-die 10)))

(defun ol-pnp-d12 ()
  "Roll a D12 die."
  (interactive)
  (message "Rolled: %s" (ol-pnp-die 12)))

(defun ol-pnp-d20 ()
  "Roll a D20 die."
  (interactive)
  (message "Rolled: %s" (ol-pnp-die 20)))

(defun ol-pnp-d100 ()
  "Roll a D100 die."
  (interactive)
  (message "Rolled: %s" (ol-pnp-die 100)))


(transient-define-prefix ol-pnp-transient ()
  "Transient for Ol-Pnp to control the table."
  [:description (lambda () (format "Battle Round: %s" ol-pnp-round-counter))
                
                ["Navigate"
                 ("n" "Move Down" ol-pnp-move-down-dline :transient t)
                 ("p" "Move Up" ol-pnp-move-up-dline :transient t)
                 ("i" "Next" ol-pnp-increment-round-indicator :transient t)
                 ("I" "Init. Battle" ol-pnp-init-round-indicator :transient t)]
                ["Extra"
                 ("A" "Apply Damag" ol-pnp-apply-damage :transient t)
                 ("H" "Show Help" ol-pnp-get-all-info-from-link :transient t)
                 ("M" "Add Monster" (lambda () (interactive) (ol-pnp-add-monster) (ol-pnp-transient)) :transient nil)
                 ("^" "Sort Order" ol-pnp-sort-by-initative :transient t)]
                ["Set Stats"
                 ("si" "Initiative" ol-pnp-set-initiative :transient t)
                 ("sh" "HP" ol-pnp-set-hp :transient t)
                 ("sm" "Max. HP" ol-pnp-set-max-hp :transient t)
                 ("sH" "Max. HP/HP" ol-pnp-set-max-hp-and-hp :transient t)
                 ("lh" "HP from link" ol-pnp-fill-hp-from-link :transient t)
                 ("ss" "Status" ol-pnp-add-status :transient t)
                 ("sn" "Notes" ol-pnp-add-notes :transient t)]
                ["Dice Rolls"
                 ("d4" "  d4" ol-pnp-d4 :transient t)
                 ("d6" "  d6" ol-pnp-d6 :transient t)
                 ("d10" " d10" ol-pnp-d10 :transient t)
                 ("d12" " d12" ol-pnp-d12 :transient t)
                 ("d20" " d20" ol-pnp-d20 :transient t)
                 ("dp" "  d100" ol-pnp-d100 :transient t)]])

(provide 'ol-pnp)
;;; ol-pnp.el ends here
