;;; eglot-tempo.el --- Use eglot as inline template expander -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 Jeffrey Walsh

;; Author: Jeff Walsh <fejfighter@gmail.com>
;; Created: 2022
;; Version: 0.5
;; Package-Requires: ((eglot "1.9")  (tempo "0.5") (emacs "24.4"))
;; Keywords: convenience, languages, tools
;; URL: https://github.com/fejfighter/eglot-tempo

;;; Commentary:

;; LSP can provide inline/template hinting for function completion,
;; Currently eglot only checks for yasnippet to enable this
;; feature.  This package patches the function or mocks yasnippet
;; (depending on what is installed) to use tempo.el for that feature
;; by translating the LSP template into a sexp used by tempo.el

(require 'tempo)
(require 'eglot)

;;; Code:
(defun eglot-tempo--convert (snippet)
  "Convert a SNIPPET returned from Eglot into a format usefful for tempo."
  (if (string-match "\\(\${\\([1-9]\\):\\([^}]*\\)}\\)\\|\\(\$[1-9]\\)\\|\\(\$0\\)\\|\\(\\.\\.\\.\\)" snippet 0)
      (cond
       ((match-string 1 snippet)
	(append `(,(substring snippet 0 (match-beginning 0))
  		  ,(list 'p (concat (match-string 3 snippet) ": ") (match-string 2 snippet)))
		(eglot-tempo--convert (substring snippet (match-end 0)))))
       ((match-string 4 snippet)
	(append `(,(substring snippet 0 (match-beginning 0)) p)
		(eglot-tempo--convert (substring snippet (match-end 0)))))
       ((match-string 5 snippet)
	(append (list (substring snippet 0 (match-beginning 0)) 'q)
		(let ((rest (substring snippet (match-end 0))))
		  (if (= (length rest) 0) ()
		    (list rest)))))
       ((match-string 6 snippet)
	(append `(, (substring snippet 0 (match-beginning 0))
		    ,(list 'p "...: "))
		(eglot-tempo--convert (substring snippet (match-end 0))))))
    (list snippet)))

(defun eglot-tempo-expand-yas-snippet (snippet &optional START END EXPAND-ENV)
  "Emulate yasnippet expansion function call.
SNIPPET - snippet for converting.
START END EXPAND-ENV are all ignored."
  (ignore START END EXPAND-ENV)
  (tempo-define-template "eglot-tempo-template" (eglot-tempo--convert snippet))
  (tempo-insert-template 'tempo-template-eglot-tempo-template nil))

(defun eglot-tempo--snippet-expansion-fn ()
  "An override of ‘eglot--snippet-expansion-fn’."
  #'eglot-tempo-expand-yas-snippet)

;;;###autoload
(define-minor-mode eglot-tempo-mode
  "Toggle eglot template support by tempo."
  :global t
  (if eglot-tempo-mode
      (advice-add #'eglot--snippet-expansion-fn
                  :override #'eglot-tempo--snippet-expansion-fn)
    (advice-remove #'eglot--snippet-expansion-fn
                   #'eglot-tempo--snippet-expansion-fn)))

(provide 'eglot-tempo)
;;; eglot-tempo.el ends here
